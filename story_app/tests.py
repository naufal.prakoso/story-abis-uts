import time
import unittest
import os
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from story_app.views import index
from story_app.models import StatusModel
from story_project.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):

    def test_app_url_is_exist(self):
        self.client = Client()
        response = self.client.get('')
        self.assertEqual(response.status_code,200)
	
    def test_app_url_is_not_exist(self):
        self.client = Client()
        response = self.client.get('/none/')
        self.assertEqual(response.status_code,404)

    def test_app_template_is_correct (self):
        self.client = Client()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_form(self):
        data = {
            'status' : "Test 123",
            'date' : "2019-01-01 00:00:00",
        }
        response = Client().post("", data)
        self.assertEqual(response.status_code, 302)
        
    def test_model_status(self):
        status_test = StatusModel(
            status = "Test 123",
            date = "2019-01-01 00:00:00",
        ) 
        status_test.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)

class FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest,self).tearDown()

    def test_opening_browser_user_see_title(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"")
        self.assertIn("Prakosite!",selenium.title)

    def test_user_sees_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        header = selenium.find_element_by_tag_name('h1').text
        self.assertEquals('HI, HOW ARE YOU?', header)

    def test_inputting_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        message = "Test"
        self.assertNotIn(message,selenium.page_source)
        text_area = selenium.find_element_by_name("status")
        text_area.send_keys(message)
        sub_button = selenium.find_element_by_id("button")
        sub_button.click()
        self.assertIn(message,selenium.page_source)

    def test_button_post_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol = "<button"
        self.assertIn(tombol,selenium.page_source)

    def test_input_form_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        kolom_status = "<textarea"
        self.assertIn(kolom_status,selenium.page_source)