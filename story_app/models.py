from django.db import models
import django.utils.timezone

# Create your models here.
class StatusModel(models.Model):
    status = models.TextField(max_length=100)
    date = models.DateTimeField(auto_now=True, blank=True)